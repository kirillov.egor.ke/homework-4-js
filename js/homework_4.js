// Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
//     При вызове функция должна спросить у вызывающего имя и фамилию.
//     Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
//     Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя,
//     соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin().
// Вывести в консоль результат выполнения функции.

// Теория. Метод объекта (функция) - это такое же обычное свойсво объекта,
// но при этом он имеет доступ к простым свойствам (не функциям) и может их использовать или изменять.

function createNewUser() {
    const userFirstName = prompt('Как тебя зовут?');
    const userLastName = prompt('Какая у тебя фамилия?');
    const newUser = {
        firstName: userFirstName,
        lastName: userLastName,
        getLogin: function () {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        }
    }
    return newUser;
}

const user = createNewUser();

console.log(user.getLogin());